package test.opencart;
import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;


public class TestMacBook extends ConditionsClass {




    @Test(priority = 1)
        public void addProductTest(){

            HomePage homePage = new HomePage(driver);
            homePage.addProduct();

            HomePage HomeOpenProduct = new HomePage(driver);
            HomeOpenProduct.open("MacBook");

            MacBookPage productName = new MacBookPage(driver);
            assertEquals("MacBook", productName.getProductName());

        }


        @Test(priority = 2)
        public void openBrandTest(){

            MacBookPage macBookBrand = new MacBookPage(driver);
            macBookBrand.brand();

            ApplePage nameOfProductManufacturer = new ApplePage(driver);
            assertEquals("Apple", nameOfProductManufacturer.GetNameOfProductManufacturer());


        }


        @Test(priority = 3)
        public void enterToCartTotalTest(){

            ApplePage appleAddProduct = new ApplePage(driver);
            appleAddProduct.addProduct();
            HeaderPage headerToCartTotal = new HeaderPage(driver);
            headerToCartTotal.ToCartTotal();
            HeaderPage headerNumInCartTotal = new HeaderPage(driver);
            assertEquals("x 2",headerNumInCartTotal.getNumInCartTotal());

        }


        @Test(priority = 4)
        public void enterToViewCartTest(){


            HeaderPage headerToViewCart = new HeaderPage(driver);
            headerToViewCart.ToViewCart();
//            assertEquals("Apple", brandPage.getBrandName());
            ViewCartPage viewCartInputQnt = new ViewCartPage(driver);

            assertEquals("2", viewCartInputQnt.getProdQnt());
        }


        @Test(priority = 5)
        public void doDoubleClickTest(){

            ViewCartPage viewCartPageDoubleClickInput = new ViewCartPage(driver);
            viewCartPageDoubleClickInput.DoubleClickInput();

            ViewCartPage viewCartPageUpdateInput = new ViewCartPage(driver);
            viewCartPageUpdateInput.update();
//            assertEquals("Apple", brandPage.getBrandName());

        }



}
