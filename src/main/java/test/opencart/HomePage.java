package test.opencart;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

public class HomePage extends PageObject {

    public HomePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@class='caption' and .//h4[. ='MacBook']]//following-sibling::div//span")
    @CacheLookup
    private  WebElement addProductXPath;




    public void addProduct(){

        act(addProductXPath, driver);

    }

    public  void open(String ProductName){


        WebElement openProductXPath =  driver.findElement(By.xpath("//h4/a[. ='"+ProductName+"']"));

        act(openProductXPath, driver);

    }
}
