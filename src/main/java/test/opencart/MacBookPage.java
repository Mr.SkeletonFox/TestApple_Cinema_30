package test.opencart;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

public class MacBookPage extends PageObject {
    public MacBookPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//li[text()='Brand: ']/a")
    @CacheLookup
    private  WebElement brandXPath;


    @FindBy(xpath = "//div/h1[.='MacBook']")
    @CacheLookup
    private  WebElement productName;

    public  void brand(){

        act(brandXPath, driver);

    }


    public String getProductName(){

    return productName.getText();

    }
}
