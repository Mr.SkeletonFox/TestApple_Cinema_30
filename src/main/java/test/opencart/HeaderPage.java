package test.opencart;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

public class HeaderPage extends PageObject{
    public HeaderPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = "//span[@id='cart-total']")
    @CacheLookup
    private WebElement toCartTotal;


    @FindBy(xpath = "//strong[.=' View Cart']")
    @CacheLookup
    private  WebElement toViewCart;

    @FindBy(xpath = "(//td[@class='text-right'])[1]")
    @CacheLookup
    private  WebElement numInCartTotal;

    public void ToCartTotal(){
        waiting();
        act(toCartTotal, driver);

    }

    public  void ToViewCart() {
        waiting();
        act(toViewCart, driver);

    }


    public String getNumInCartTotal() {

        return numInCartTotal.getText();

    }
}
