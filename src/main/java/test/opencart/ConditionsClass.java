package test.opencart;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class ConditionsClass {

    protected static WebDriver driver;


    @BeforeClass
    public static void enterToSite() {

        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.navigate().to("https://demo.opencart.com");

    }


    @AfterClass
    public void closeDriver() {

        driver.get("https://demo.opencart.com/index.php?route=common/home");
        driver.close();
        driver.quit();
    }
}
