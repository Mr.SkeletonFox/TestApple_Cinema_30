package test.opencart;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

public class ViewCartPage extends PageObject {

    public ViewCartPage(WebDriver driver) {
        super(driver);
    }


    @FindBy(xpath = "//td/div/input[@class='form-control']")
    @CacheLookup
    private  WebElement input;


    @FindBy(xpath = "//button[@type='submit']")
    @CacheLookup
    private  WebElement updateButton;

    @FindBy(xpath = "(//input[@class='form-control'])[1]")
    @CacheLookup
    private  WebElement viewCartInputQnt;
    public void DoubleClickInput(){



        Actions actions = new Actions(driver);
        actions.moveToElement(input).doubleClick().sendKeys("1").perform();



    }

    public void update(){


        act(updateButton, driver);

    }

    public String getProdQnt() {
        return viewCartInputQnt.getAttribute("value");
    }
}
