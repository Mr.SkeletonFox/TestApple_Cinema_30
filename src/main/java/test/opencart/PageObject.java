package test.opencart;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageObject {


    protected WebDriver driver;
    public PageObject(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


//    public static String[] xpath = new String[]{"//div[@class='caption' and .//h4[. ='MacBook']]//following-sibling::div//span",
//                                                "//h4/a[. ='MacBook']",
//                                                "//li[text()='Brand: ']/a",
//                                                "//span[@id='cart-total']",
//                                                "//strong[.=' View Cart']",
//                                                "//td/div/input[@class='form-control']",
//                                                "//button[@type='submit']"};




    public void act(WebElement element, WebDriver driver) {

        Actions actions = new Actions(driver);
        actions.moveToElement(element).click().perform();

    }


    public void waiting(){
//        final Wait<WebDriver> wait = new WebDriverWait(driver, 100, 1000000);
//        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Wait wait = new WebDriverWait(driver, 3);
        wait.until(driver -> {
            JavascriptExecutor jse = (JavascriptExecutor) driver;
            String active = jse.executeScript("return window.jQuery.active").toString();
            return active.equals("0");
        });


    }




}
