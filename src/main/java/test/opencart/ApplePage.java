package test.opencart;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import static org.testng.Assert.assertTrue;

public class ApplePage extends PageObject{

    @FindBy(xpath = "//div[@class='caption' and .//h4[. ='MacBook']]//following-sibling::div//span")
    @CacheLookup
    private  WebElement addProductXPath;

    @FindBy(xpath = "//div/h2[. ='Apple']")
    @CacheLookup
    private  WebElement nameOfProductManufacturer;

    public ApplePage(WebDriver driver) {

        super(driver);
        assertTrue(addProductXPath.isDisplayed());

    }

    public void addProduct(){


        act(addProductXPath, driver);

    }

    public String GetNameOfProductManufacturer() {
        return nameOfProductManufacturer.getText();
    }
}
